import threading
import time
import logging

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s',)
indice = -1
buffer = [None,None,None,None,None]

def consumer(cv):
    item=None
    logging.debug('Consumer thread started ...')
    for _ in range(20):
        global indice
        global buffer
        with cv:
            logging.debug('Consumer waiting ...')
            cv.wait()
            item = buffer[indice]
            buffer[indice]=None
            print(f"productor {indice} {item} {buffer}", flush = True)
            indice-=1
            logging.debug('Consumer consumed the resource')

def producer(cv):
    logging.debug('Producer thread started ...')
    for n in range(20):
        global indice
        global buffer
        with cv:
            item = n*n
            logging.debug('Making resource available')
            indice+=1
            buffer[indice]=item
            print(f"productor {indice} {item} {buffer}", flush = True)
            logging.debug('Notifying to all consumers')
            cv.notifyAll()


if __name__ == '__main__':
    condition = threading.Condition()
    cs1 = threading.Thread(name='consumer1', target=consumer, args=(condition,))
    #cs2 = threading.Thread(name='consumer2', target=consumer, args=(condition,))
    pd = threading.Thread(name='producer', target=producer, args=(condition,))

    cs1.start()
    #time.sleep(2)
    #cs2.start()
    #time.sleep(2)
    pd.start()